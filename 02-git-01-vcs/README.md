# Домашнее задание к занятию «2.1. Системы контроля версий»

## Задание №1 - Создать и настроить репозиторий для дальнейшей работы на курсе.

В результате предыдущих шагов в репозитории должно быть как минимум пять коммитов (если вы еще сделали какие-нибудь промежуточные – нет проблем):
- Initial Commit – созданный гитхабом при инициализации репозитория.
- First commit for homework 2.1 - созданный после изменения файла README.md.
- Added gitignore – после обновления .gitignore.
- Prepare to delete and move – после добавления двух временных файлов.
- Moved and deleted – после удаления и перемещения временных файлов.

---

В файле README.md опишите своими словами какие файлы будут проигнорированы в будущем благодаря добавленному .gitignore:
- `**/.terraform/*` - игнорировать a/b/c/.terraform/любой файл 
- `*.tfstate` - иcключить все файлы с расширением .tfstate
- `*.tfstate.*` - исключить все файлы заканчивающиеся на .tfstate.любые символы
- `crash.log` - исключить crash.log например: logs/crash.log или foo/bar/crash.log 
- `*.tfvars` - исключить все файлы с расширением .tfvars 
- `override.tf` - исключить override.tf например: foo/override.tf или foo/bar/override.tf
- `override.tf.json` - исключить override.tf.json например: foo/override.tf.json или foo/bar/override.tf.json
- `*_override.tf` - исключить `_override.tf` например: foo_override.tf или bar_override.tf  
- `*_override.tf.json` - исключить `_override.tf.json` например: foo_override.tf.json или bar_override.tf.json  
- `.terraformrc` - исключить все файлы с расширением .terraformrc 
- `terraform.rc` - исключить terraform.rc например foo/terraform.rc или foo/bar/terraform.rc

## Задание №2 – Знакомство с документацией

Изучен вывод команд: `git --help`, `git add --help`

Заметка: 
- `-h` краткая информация
- `--help` полная информация

